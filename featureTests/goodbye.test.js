const { serverlessProcess, serverlessService } = require('./helper.js')

describe('goodbye', () => {
  beforeAll(async () => {
    // serverlessProcess.start starts serverless offline in a child process
    // unless the test is pointed at a deployed service
    await serverlessProcess.start()
  })

  afterAll(() => {
    // serverlessProcess.stop kills the child process at the end of the test
    // unless the test is pointed at a deployed service
    serverlessProcess.stop()
  })

  it('makes a request to the serverless process', async () => {
    // serverlessService is an axios instance pointed at your serverless offline
    // unless the test is pointed at a deployed service, then it uses that.
    try {
      let response = await serverlessService.get('/aives/goodbye')
    } catch (e) {
      expect(e.message).toEqual('Request failed with status code 400')
      expect(e.response.status).toEqual(400)
      expect(e.response.data.message).toEqual('This always throws an error because we hate fun')
    }
  })
});

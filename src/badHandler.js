'use strict';

module.exports.goodbye = async function(event) {
  return {
    statusCode: 400,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    body: JSON.stringify(
      {
        message: 'This always throws an error because we hate fun'
      },
      null,
      2
    ),
  };
};


'use strict';

const mod = require('./badHandler');

const jestPlugin = require('serverless-jest-plugin');
const lambdaWrapper = jestPlugin.lambdaWrapper;
const wrapped = lambdaWrapper.wrap(mod, { handler: 'goodbye' });

describe('goodbye', () => {
  it('returns parameters in the body', () => {
    return wrapped.run({}).then((response) => {
      expect(response.statusCode).toEqual(400);
      expect(JSON.parse(response.body).message).toEqual("This always throws an error because we hate fun")
    });
  });
});
